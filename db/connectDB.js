const mongoose = require("mongoose");
const { db } = require("./../config");

// const dbURI = 'mongodb://' + db.host + ':' + db.port + '/' + db.name;
const dbURI = db.atlas;
module.exports.connect = async () => {
  try {
    const conn = await mongoose.connect(dbURI);
  } catch (error) {
    process.exit(1);
  }
};
