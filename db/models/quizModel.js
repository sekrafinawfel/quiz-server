const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const quizSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    isPublished: {
      type: Boolean,
      default: false,
    },
    createdBy: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
    },
    questions: [{ type: mongoose.Schema.ObjectId, ref: "Question" }],
    categories: [{ type: mongoose.Schema.ObjectId, ref: "Category" }],
    deletedAt: Date,
  },
  {
    timestamps: true,
  }
);

quizSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Quiz", quizSchema);
