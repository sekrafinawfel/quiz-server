const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const questionSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    isMultipleChoice: {
      type: Boolean,
      default: false,
    },
    createdBy: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
    },
    choices: [{ type: mongoose.Schema.ObjectId, ref: "Choice" }],
    deletedAt: Date,
  },
  {
    timestamps: true,
  }
);

questionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Question", questionSchema);
