const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const categorySchema = new mongoose.Schema( {
    title: {
      type: String,
      trim: true,
    },
    deletedAt: Date,
  },
  {
    timestamps: true,
  })

categorySchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Category", categorySchema);
