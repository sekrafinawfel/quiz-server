const quizModel = require("../models/quizModel");
const APIFeatures = require("../../utils/apiFeatures");

module.exports = class QuizRepo {
  static findOneByObjSelect(obj, select) {
    return quizModel.findOne(obj).select(select);
  }

  static findOneByObj(obj) {
    return quizModel.findOne(obj);
  }
  static findOneByObjAndPopulate(obj, field) {
    return quizModel.findOne(obj).populate(field);
  }

  static create(payload) {
    return quizModel.create(payload);
  }

  static findById(id) {
    return quizModel.findById(id);
  }
  static findByObjAndUpdate(obj, payload) {
    return quizModel.findOneAndUpdate(obj, { $set: payload }, { new: true });
  }

  static findByIdAndUpdate(id, payload) {
    return quizModel.findByIdAndUpdate(id, { $set: payload }, { new: true });
  }

  static async findByObjPaginate(obj, options, query) {
    let deleted = query.deleted == "true" ? true : false;

    const features = new APIFeatures(
      deleted
        ? quizModel.find({ ...obj, deletedAt: { $ne: null } }).populate('questions')
        : quizModel.find({ ...obj, deletedAt: null }).populate('questions'),
      query
    )
      .filter()
      .sort()
      .limitFields()
      .search(["title", "description"]);

    let optionsPaginate = {
      limit: options.limit ? options.limit : null,
      page: options.page ? options.page : null,
    };

    const pagination = quizModel.paginate(features.query, optionsPaginate);
    return await pagination;
  }

  static async deleteById(id) {
    return quizModel
      .findByIdAndUpdate(
        id,
        {
          $set: { deletedAt: Date.now() },
        },
        { new: true }
      )
      .lean()
      .exec();
  }

  static addQuestion(id, value){
    return quizModel.findByIdAndUpdate(id, {$addToSet: {"questions": value}},  { new: true });
  }
  static removeQuestion(id, value){
    return quizModel.findByIdAndUpdate(id, {$pull: {"questions": value}},  { new: true });
  }

  static checkQuestion(quizId, questionId){
    return quizModel.findOne({_id: quizId , questions: questionId, deletedAt: null });
  }
};
