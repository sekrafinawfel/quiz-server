const questionModel = require("../models/questionModel");
const APIFeatures = require("../../utils/apiFeatures");

module.exports = class QuizRepo {
  static findOneByObjSelect(obj, select) {
    return questionModel.findOne(obj).select(select);
  }

  static findOneByObj(obj) {
    return questionModel.findOne(obj);
  }

  static create(payload) {
    return questionModel.create(payload);
  }

  static findById(id) {
    return questionModel.findById(id);
  }

  static findByObjAndUpdate(obj, payload) {
    return questionModel.findOneAndUpdate(
      obj,
      { $set: payload },
      { new: true }
    );
  }
  static findByIdAndUpdate(id, payload) {
    return questionModel.findByIdAndUpdate(
      id,
      { $set: payload },
      { new: true }
    );
  }

  static async findByObjPaginate(obj, options, query) {
    let deleted = query.deleted == "true" ? true : false;

    const features = new APIFeatures(
      deleted
        ? questionModel.find({ ...obj, deletedAt: { $ne: null } })
        : questionModel.find({ ...obj, deletedAt: null }),
      query
    )
      .filter()
      .sort()
      .limitFields()
      .search(["title", "description"]);

    let optionsPaginate = {
      limit: options.limit ? options.limit : null,
      page: options.page ? options.page : null,
    };

    const pagination = questionModel.paginate(features.query, optionsPaginate);
    return await pagination;
  }

  static async deleteById(id) {
    return questionModel
      .findByIdAndUpdate(
        id,
        {
          $set: { deletedAt: Date.now() },
        },
        { new: true }
      )
      .lean()
      .exec();
  }
};
