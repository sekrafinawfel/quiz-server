const authController = require("../../controllers/authController");
const questionController = require("../../controllers/questionController");
const express = require("express");
const router = express.Router();
const { schemaValidator } = require("../../middlewares/schemaValidator");
const {
    createQuestion,
    getQuestions,
    updateQuestion,
    checkId,
    checkIdQuiz,
    checkIdQuestion,
    checkIdQuizQuestion

  } = require("./schemas/questionSchemas");
/**
 * @swagger
 * /quizzes/{id_quiz}/questions:
 *   post:
 *     summary: question Creation
 *     parameters:
 *      - in: path
 *        name: id_quiz
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/CreateQuestion'
 *     tags: [Question]
 *     responses:
 *       200:
 *         description: question Creation
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Question'
 *     security:
 *      - bearerAuth: []
 *
 */

router.route("/:id_quiz/questions").post(
    // authController.protect,
    schemaValidator(createQuestion),
    schemaValidator(checkIdQuiz, "params"),
    questionController.createQuestion
  );
  
  /**
   * @swagger
   * /quizzes/{id_quiz}/questions:
   *   get:
   *     summary: Returns the list of all the questions by quiz id
   *     tags: [Question]
   *     parameters:
   *        - in: path
   *          name: id_quiz
   *     responses:
   *       200:
   *         description: The list of the questions
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                  $ref: '#/components/schemas/Question'
   *     security:
   *      - bearerAuth: []
   */
  
  router.route("/:id_quiz/questions/").get(
    // authController.protect,
    schemaValidator(checkIdQuiz, "params"),
    questionController.getQuestions
  );
  
  /**
   * @swagger
   * /quizzes/{id_quiz}/questions/{id_question}:
   *   get:
   *     summary: Get one Question by id
   *     tags: [Question]
   *     parameters:
   *      - in: path
   *        name: id_quiz
   *      - in: path
   *        name: id_question
   *     responses:
   *       200:
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               $ref: '#/components/schemas/Question'
   *     security:
   *      - bearerAuth: []
   */
  
  router.route("/:id_quiz/questions/:id_question").get(
    // authController.protect,
    schemaValidator(checkIdQuizQuestion, "params"),
    questionController.getQuestion
  );
  /**
   * @swagger
   * /quizzes/{id_quiz}/questions/{id_question}:
   *   put:
   *     summary: update one question by id
   *     tags: [Question]
   *     parameters:
   *      - in: path
   *        name: id_quiz
   *      - in: path
   *        name: id_question
   *     requestBody:
   *         required: true
   *         content:
   *            application/json:
   *                schema:
   *                   $ref: '#/components/schemas/UpdateQuestion'
   *     responses:
   *       200:
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               $ref: '#/components/schemas/Question'
   *     security:
   *      - bearerAuth: []
   */
  
  router.put(
    "/:id_quiz/questions/:id_question",
    // authController.protect,
    schemaValidator(checkIdQuizQuestion, "params"),
    schemaValidator(updateQuestion),
    questionController.updateQuestion
  );
  
  /**
   * @swagger
   * /quizzes/{id_quiz}/questions/{id_question}:
   *   delete:
   *     summary: delete one question by id
   *     tags: [Question]
   *     parameters:
   *      - in: path
   *        name: id_quiz
   *      - in: path
   *        name: id_question
   *     responses:
   *       200:
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               $ref: '#/components/schemas/Question'
   *     security:
   *      - bearerAuth: []
   */
  
  router.route( "/:id_quiz/questions/:id_question").delete(
    // authController.protect,
    schemaValidator(checkIdQuizQuestion, "params"),
    questionController.deleteQuestion
  );

  module.exports = router;
  