const express = require("express");
const router = express.Router();

const authRoutes = require("./authRoutes");
const userRoutes = require("./userRoutes");
const quizRoutes = require("./quizRoutes");
const questionRoutes = require("./questionRoutes");

router.use("/", authRoutes);
router.use("/users", userRoutes);
router.use("/quizzes", quizRoutes);
router.use("/quizzes", questionRoutes);

module.exports = router;
