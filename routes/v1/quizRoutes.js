const express = require("express");
const router = express.Router();
const quizController = require("../../controllers/quizController")
const authController = require("../../controllers/authController");
const { createQuiz, getQuizzes, updateQuiz, checkId } = require("./schemas/quizSchemas");
const { schemaValidator } = require("../../middlewares/schemaValidator");

/**
 * @swagger
 * tags:
 *   name: Quiz
 *   description: The Quiz managing API
 */

/**
 * @swagger
 * /quizzes:
 *   post:
 *     summary: quiz Creation
 *     requestBody:
 *        required: true
 *        content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/CreateQuiz'
 *     tags: [Quiz]
 *     responses:
 *       200:
 *         description: Quiz creation
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Quiz'
 *     security:
 *      - bearerAuth: []
 *
 */

router.post(
  "/",
  // authController.protect,
  schemaValidator(createQuiz),
  quizController.createQuiz
);

/**
 * @swagger
 * /quizzes:
 *   get:
 *     summary: Returns the list of all the quizzes
 *     tags: [Quiz]
 *     parameters:
 *        - in: query
 *          name: search
 *          schema:
 *            type: string
 *        - in: query
 *          name: deleted
 *          schema:
 *            type: string
 *        - in: query
 *          name: sort
 *          schema:
 *            type: string
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *        - in: query
 *          name: perPage
 *          schema:
 *            type: integer
 *     responses:
 *       200:
 *         description: The list of the quizzes
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Quiz'
 *     security:
 *      - bearerAuth: []
 */

router.get(
  "/",
  // authController.protect,
  schemaValidator(getQuizzes, "query"),
  quizController.getQuizzes
);
/**
 * @swagger
 * /quizzes/{id}:
 *   get:
 *     summary: Get one Quiz by id
 *     tags: [Quiz]
 *     parameters:
 *      - in: path
 *        name: id
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Quiz'
 *     security:
 *      - bearerAuth: []
 */

router.route("/:id").get(
  // authController.protect,
  schemaValidator(checkId, "params"),
  quizController.getQuiz
);
/**
 * @swagger
 * /quizzes/{id}:
 *   put:
 *     summary: update one quiz by id
 *     tags: [Quiz]
 *     parameters:
 *      - in: path
 *        name: id
 *     requestBody:
 *         required: true
 *         content:
 *            application/json:
 *                schema:
 *                   $ref: '#/components/schemas/UpdateQuiz'
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Quiz'
 *     security:
 *      - bearerAuth: []
 */

router.put(
  "/:id",
  // authController.protect,
  schemaValidator(checkId, "params"),
  schemaValidator(updateQuiz),
  quizController.updateQuiz
);
/**
 * @swagger
 * /quizzes/{id}:
 *   delete:
 *     summary: delete one quiz by id
 *     tags: [Quiz]
 *     parameters:
 *      - in: path
 *        name: id
 *     responses:
 *       200:
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Question'
 *     security:
 *      - bearerAuth: []
 */

router.delete(
  "/:id",
  // authController.protect,
  schemaValidator(checkId, "params"),
  quizController.deleteQuiz
);

module.exports = router;
