const Joi = require("joi");
const { JoiObjectId } = require("../../../middlewares/schemaValidator");
/**
 * @swagger
 * components:
 *   securitySchemes:
 *      bearerAuth:
 *          type: http
 *          scheme: bearer
 *          bearerFormat: JWT
 */
/**
 * @swagger
 * components:
 *   schemas:
 *     Quiz:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *           description: The title
 *         description:
 *           type: string
 *           description: the description
 *         categories:
 *           type: array
 *           items:
 *             type: string
 *           description: array of categories
 *         isPublished:
 *           type: boolean
 *           description: publish or unpublish a quiz, this is available for admin and super admin only
 *     CreateQuiz:
 *       type: object
 *       required:
 *         - title
 *         - description
 *       properties:
 *         title:
 *           type: string
 *           description: The title
 *         description:
 *           type: string
 *           description: the description
 *         categories:
 *           type: array
 *           items:
 *             type: string
 *           description: array of categories
 *         isPublished:
 *           type: boolean
 *           description: publish or unpublish a quiz, this is available for admin and super admin only
 *     UpdateQuiz:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *           description: The title
 *         description:
 *           type: string
 *           description: the description
 *         isPublished:
 *           type: boolean
 *           description: publish or unpublish a quiz
 */

exports.createQuiz = Joi.object({
  title: Joi.string().trim().min(3).max(500).required(),
  description: Joi.string().trim().min(3).max(3000).required(),
  isPublished: Joi.boolean(),
  categories: Joi.array().items(JoiObjectId()).optional(),
});

exports.updateQuiz = Joi.object({
  title: Joi.string().trim().min(3).max(500).optional(),
  description: Joi.string().trim().min(3).max(3000).optional(),
  isPublished: Joi.boolean().optional(),
  categories: Joi.array().items(JoiObjectId()).optional(),
});

exports.getQuizzes = Joi.object({
  title: Joi.string().trim().min(3).max(30).optional(),
  description: Joi.string().trim().min(3).max(1000).optional(),
  deleted: Joi.boolean().optional(),
  page: Joi.number().optional(),
  perPage: Joi.number().optional(),
  search: Joi.string().optional(),
  sort: Joi.string().optional(),
});

exports.checkId = Joi.object({
  id: JoiObjectId().required(),
});
