const Joi = require("joi");
const { JoiObjectId } = require("../../../middlewares/schemaValidator");
/**
 * @swagger
 * components:
 *   securitySchemes:
 *      bearerAuth:
 *          type: http
 *          scheme: bearer
 *          bearerFormat: JWT
 */
/**
 * @swagger
 * components:
 *   schemas:
 *     Question:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *           description: The title
 *         description:
 *           type: string
 *           description: the description
 *         isMultipleChoice:
 *           type: boolean
 *           description: is multiple choice or not
 *         choices:
 *           type: array
 *           items:
 *             type: string
 *           description: array of choices
 *     CreateQuestion:
 *       type: object
 *       required:
 *         - title
 *         - description
 *       properties:
 *         title:
 *           type: string
 *           description: The title
 *         description:
 *           type: string
 *           description: the description
 *         isMultipleChoice:
 *           type: boolean
 *           description: is multiple choice or not
 *         choices:
 *           type: array
 *           items:
 *             type: string
 *           description: array of choices
 *     UpdateQuestion:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *           description: The title
 *         description:
 *           type: string
 *           description: the description
 *         isMultipleChoice:
 *           type: boolean
 *           description: is multiple choice or not
 *         choices:
 *           type: array
 *           items:
 *             type: string
 *           description: array of choices
 */

exports.createQuestion = Joi.object({
  title: Joi.string().trim().min(3).max(500).required(),
  description: Joi.string().trim().min(3).max(3000).required(),
  isMultipleChoice: Joi.boolean().optional(),
});
exports.updateQuestion = Joi.object({
  title: Joi.string().trim().min(3).max(500).optional(),
  description: Joi.string().trim().min(3).max(3000).optional(),
  isMultipleChoice: Joi.boolean().optional(),
});
exports.checkId = Joi.object({
  id: JoiObjectId().required(),
});
exports.checkIdQuiz = Joi.object({
  id_quiz: JoiObjectId().required(),
});
exports.checkIdQuestion = Joi.object({
  id_question: JoiObjectId().required(),
});
exports.checkIdQuizQuestion = Joi.object({
  id_question: JoiObjectId().required(),
  id_quiz: JoiObjectId().required(),
});
