const asyncHandler = require("express-async-handler");
const { BadRequestError, NotFoundError } = require("../middlewares/apiError");
const {
  SuccessMsgResponse,
  SuccessResponse,
  SuccessMsgDataResponse,
  SuccessResponsePagination,
} = require("../middlewares/apiResponse");

const QuizRepo = require("../db/repositories/quizRepo");

exports.createQuiz = asyncHandler(async (req, res) => {
  let quiz = await QuizRepo.create({ ...req.body });

  return new SuccessMsgDataResponse(quiz, "Quiz created successfully").send(
    res
  );
});

exports.getQuizzes = asyncHandler(async (req, res) => {
  const { page, perPage } = req.query;
  const options = {
    page: parseInt(page, 10) || 1,
    limit: parseInt(perPage, 10) || 10,
  };
  const quizzes = await QuizRepo.findByObjPaginate({}, options, req.query);
  if (!quizzes) {
    return new SuccessMsgResponse("No quiz found").send(res);
  }
  const { docs, ...meta } = quizzes;

  return new SuccessResponsePagination(docs, meta).send(res);
});

exports.getQuiz = asyncHandler(async (req, res) => {
  const quiz = await QuizRepo.findOneByObjAndPopulate({
    _id: req.params.id,
    deletedAt: null,
  }, 'questions');
  if (!quiz) {
    throw new NotFoundError("Quiz not found");
  }
  return new SuccessResponse(quiz).send(res);
});

exports.updateQuiz = asyncHandler(async (req, res) => {
  const quiz = await QuizRepo.findByObjAndUpdate(
    {
      _id: req.params.id,
      deletedAt: null,
    },
    req.body
  );
  if (!quiz) {
    throw new NotFoundError("Quiz not found");
  }

  return new SuccessMsgDataResponse(quiz, "Quiz updated successfully").send(
    res
  );
});

exports.deleteQuiz = asyncHandler(async (req, res) => {
  const quiz = await QuizRepo.findOneByObj({
    _id: req.params.id,
    deletedAt: null,
  });
  if (!quiz) {
    throw new NotFoundError("Quiz not found");
  }
  let deletedQuiz = await QuizRepo.deleteById(quiz.id);
  return new SuccessMsgDataResponse(
    deletedQuiz,
    "Quiz deleted successfully"
  ).send(res);
});
