const asyncHandler = require("express-async-handler");
const { BadRequestError, NotFoundError } = require("../middlewares/apiError");
const {
  SuccessMsgResponse,
  SuccessResponse,
  SuccessMsgDataResponse,
  SuccessResponsePagination,
} = require("../middlewares/apiResponse");

const QuestionRepo = require("../db/repositories/questionRepo");
const QuizRepo = require("../db/repositories/quizRepo");

exports.createQuestion = asyncHandler(async (req, res) => {
  const quizId = req.params.id_quiz;
  
  const quiz = await QuizRepo.findById(quizId);
  if(!quiz){
    throw new NotFoundError("Quiz not found");
  }

  const question = await QuestionRepo.create({
    ...req.body,
  });
  const updatedQuiz =  await QuizRepo.addQuestion(quizId, question.id);
  if(!updatedQuiz){
    throw new NotFoundError("Quiz not found");
  }
  return new SuccessMsgDataResponse(
    question,
    "Question created successfully"
  ).send(res);
});

exports.getQuestions = asyncHandler(async (req, res) => {
  const quizId = req.params.id_quiz;
  const quiz = await QuizRepo.findOneByObjAndPopulate({
    _id: quizId,
    deletedAt: null,
  }, 'questions');
  if(!quiz){
    throw new NotFoundError("Quiz not found");
  }

  return new SuccessResponse(quiz.questions).send(res);
});

exports.getQuestion = asyncHandler(async (req, res) => {
  const quizId = req.params.id_quiz;
  const questionId = req.params.id_question;

  const quiz = await QuizRepo.findById(quizId);
  if(!quiz){
    throw new NotFoundError("Quiz not found");
  }

  const question = await QuestionRepo.findById(questionId);
  if (!question){
    throw new NotFoundError("Question not found");
  }

  const quiz2 = await QuizRepo.checkQuestion(quizId,questionId);
  if(!quiz2){
    throw new NotFoundError("Question does not belong to this Quiz");
  }

  return new SuccessResponse(question).send(res);
});

exports.updateQuestion = asyncHandler(async (req, res) => {
  const quizId = req.params.id_quiz;
  const questionId = req.params.id_question;

  const quiz = await QuizRepo.findById(quizId);
  if(!quiz){
    throw new NotFoundError("Quiz not found");
  }

  const question = await QuestionRepo.findById(questionId);
  if (!question) {
    throw new NotFoundError("Question not found");
  }

  const quiz2 = await QuizRepo.checkQuestion(quizId,questionId);
  if(!quiz2){
    throw new NotFoundError("Question does not belong to this Quiz");
  }
  
  const updatedQuestion = await QuestionRepo.findByObjAndUpdate(
    {
      _id: questionId,
      deletedAt: null,
    },
    req.body
  );
 

  return new SuccessMsgDataResponse(
    updatedQuestion,
    "Question updated successfully"
  ).send(res);
});

exports.deleteQuestion = asyncHandler(async (req, res) => {
  const quizId = req.params.id_quiz;
  const questionId = req.params.id_question;

  const quiz = await QuizRepo.findById(quizId);
  if(!quiz){
    throw new NotFoundError("Quiz not found");
  }

  const question = await QuestionRepo.findByObjAndUpdate({
    _id: questionId,
    deletedAt: null,
  },
  { deletedAt: Date.now() },
  );
  if (!question) {
    throw new NotFoundError("Question not found");
  }

  const quiz2 = await QuizRepo.checkQuestion(quizId,questionId);
  if(!quiz2){
    throw new NotFoundError("Question does not belong to this Quiz");
  }

  await QuizRepo.removeQuestion(quizId, questionId);

  return new SuccessMsgDataResponse(
    question,
    "Question deleted successfully"
  ).send(res);
});
