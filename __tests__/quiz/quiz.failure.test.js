const request = require("supertest");
const app = require("../../app");
const { db } = require("../../config");
const mongoose = require("mongoose");

const testDB_URI = db.atlas_test


/* Connecting to the database before all test. */
beforeAll(async () => {
  await mongoose.connect(testDB_URI
  );
});

/* Closing database connection after all test. */
afterAll(async () => {
  await mongoose.connection.close();
});

describe("QUIZ - FAILURE", () => {
  describe("GET A QUIZ", () => {
    describe("not found case", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const {statusCode, _body} = await request(app).get(`/api/v1/quizzes/${id}`);
        expect(statusCode).toBe(404);
        expect(_body.status).toBe('fail');
        expect(_body.message).toBe('Quiz not found');
      });
    });
  });
  describe("POST NEW QUIZ", () => {
    describe("validation error (joi)", () => {
      it("should return a validation error", async () => {
        const { body, status } = await request(app)
          .post("/api/v1/quizzes")
          .send({
            title: "Quiz 2",
            description: "this is a quiz description",
            isPublished: "unvalid value",
          });
        expect(status).toBe(422);
        expect(body.message).toBe("isPublished must be a boolean");
        expect(body.status).toBe("fail");
      });
    });
  });
  describe("UPDATE A QUIZ", () => {
    describe("not found case", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app)
          .put(`/api/v1/quizzes/${id}`)
          .send({ title: "updated Quiz" });
          expect(statusCode).toBe(404);
          expect(_body.status).toBe('fail');
          expect(_body.message).toBe('Quiz not found');
      });
    });
    describe("validation error (joi)", () => {
      it("should return a validation error", async () => {
        const id = new mongoose.Types.ObjectId();
        const { body, status } = await request(app)
          .put(`/api/v1/quizzes/${id}`)
          .send({ isPublished: "unvalid value" });
         expect(status).toBe(422);
         expect(body.message).toBe("isPublished must be a boolean");
         expect(body.status).toBe("fail");
      });
    });
  });
  describe("DELETE A QUIZ", () => {
    describe("not found case", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { _body ,statusCode } = await request(app).delete(
          `/api/v1/quizzes/${id}`
        );
        expect(statusCode).toBe(404);
        expect(_body.status).toBe('fail');
        expect(_body.message).toBe('Quiz not found');
      });
    });
  });
});
