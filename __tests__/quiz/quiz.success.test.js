const request = require("supertest");
const app = require("../../app");
const { db } = require("../../config");
const mongoose = require("mongoose");

const testDB_URI = db.atlas_test

var quizId;


/* Connecting to the database before all test. */
beforeAll(async () => {
  await mongoose.connect(testDB_URI
  );
});

/* Closing database connection after all test. */
afterAll(async () => {
  await mongoose.connection.close();
});


describe("QUIZ - SUCCESS", () => {
  it("POST/ quiz - success", async () => {
    const payload = {
      title: "Quiz 2",
      description: "this is a quiz description",
      isPublished: false,
      categories: [],
    };
    const { statusCode, body } = await request(app)
      .post("/api/v1/quizzes")
      .send(payload)
      .set("Content-type", "application/json");
    expect(statusCode).toBe(200);
    expect(body.status).toBe("success");
    expect(body.message).toBe("Quiz created successfully");
    expect(body.data).toMatchObject({
      title: "Quiz 2",
      description: "this is a quiz description",
      isPublished: false,
      questions: [],
      categories: [],
    });

    quizId = body.data._id;
  });
  it("GET/ quizzes - success", async () => {
    const { _body, statusCode } = await request(app).get("/api/v1/quizzes");
    expect(statusCode).toBe(200);
    expect(_body.status).toBe("success");
    expect(Array.isArray(_body.data)).toBe(true);
  });

  it("GET/ quiz - success", async () => {
    const id = quizId;
    const { body, status } = await request(app).get(`/api/v1/quizzes/${id}`);
    expect(status).toBe(200);
    expect(body.status).toBe("success");
    expect(body.data).toMatchObject({
      _id: quizId,
      title: "Quiz 2",
      description: "this is a quiz description",
      isPublished: false,
      questions: [],
      categories: [],
    });
  });

  it("PUT/ quiz - success", async () => {
    const id = quizId;
    const payload = {
      title: "updated title",
      isPublished: true,
    };
    const { statusCode, _body } = await request(app)
      .put(`/api/v1/quizzes/${id}`)
      .send(payload)
      .set("Content-type", "application/json");
    expect(statusCode).toBe(200);
    expect(_body.status).toBe("success");
    expect(_body.message).toBe("Quiz updated successfully");
    expect(_body.data).toMatchObject({
      title: "updated title",
      description: "this is a quiz description",
      isPublished: true,
      questions: [],
      categories: [],
    });
  });
  it("DELETE/ quiz - success", async () => {
    const id = quizId;
    const { _body, statusCode } = await request(app).delete(
      `/api/v1/quizzes/${id}`
    );
    expect(statusCode).toBe(200);
    expect(_body.status).toBe("success");
    expect(_body.message).toBe("Quiz deleted successfully");
  });
});
