const request = require("supertest");
const app = require("../../app");
const { db } = require("../../config");
const mongoose = require("mongoose");

const testDB_URI = db.atlas_test

var quizId;
var quizId2;
var questionId ;


/* Connecting to the database before all test. */
beforeAll(async () => {
  await mongoose.connect(testDB_URI
  );
});

/* Closing database connection after all test. */
afterAll(async () => {
  await mongoose.connection.close();
});

describe("QUESTION  - FAILURE", () => {
  describe("GET A QUESTION", () => {   
    describe("not found case of Quiz", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app).get(
          `/api/v1/quizzes/${id}/questions/${id}`
        );
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Quiz not found");
      });
    });
    describe("not found case of Question", () => {
      it("should return not found", async () => {
        const payloadQuiz = {
          title: "Quiz 2",
          description: "this is a quiz description",
          isPublished: false,
          categories: [],
        };
        const answer = await request(app)
        .post("/api/v1/quizzes")
        .send(payloadQuiz)
        .set("Content-type", "application/json");
  
        quizId = answer.body.data._id;

        const id = new mongoose.Types.ObjectId();

        const { statusCode , _body} = await request(app).get(
          `/api/v1/quizzes/${quizId}/questions/${id}`
        );
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Question not found");
      });
    });
    describe("Question does not belong to this Quiz", () => {
      it("should return Question does not belong to this Quiz", async () => {
        const payloadQuiz2 = {
          title: "Quiz 2",
          description: "this is a quiz description",
          isPublished: false,
          categories: [],
        };

         const answer2 = await request(app)
        .post("/api/v1/quizzes")
        .send(payloadQuiz2)
        .set("Content-type", "application/json");

        quizId2 = answer2.body.data._id;

        const payloadQuestion = {
          title: "question 1",
          description: "this is a question description",
          isMultipleChoice: false
        };
        const { body } = await request(app)
          .post(`/api/v1/quizzes/${quizId}/questions`)
          .send(payloadQuestion)
          .set("Content-type", "application/json");

          questionId = body.data._id;

        const { statusCode, _body } = await request(app)
          .delete(`/api/v1/quizzes/${quizId2}/questions/${questionId}`);
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Question does not belong to this Quiz");
      });
    });

  });
  describe("POST NEW QUESTION", () => {
    describe("not found case of Quiz", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app)
        .post(`/api/v1/quizzes/${id}/questions`)
        .send({
          title: "question 4",
          description: "this is a question description",
          isMultipleChoice: false,
        });
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Quiz not found");
      });
    });

    describe("validation error (joi)", () => {
      it("should return a validation error", async () => {
        const id = new mongoose.Types.ObjectId();
        const { body, status } = await request(app)
          .post(`/api/v1/quizzes/${id}/questions`)
          .send({
            title: "question 4",
            description: "this is a question description",
            isMultipleChoice: "unvalid value",
          });
        expect(status).toBe(422);
        expect(body.message).toBe("isMultipleChoice must be a boolean");
        expect(body.status).toBe("fail");
      });
    });
  });
  describe("UPDATE A QUESTION", () => {
    describe("Question does not belong to this Quiz", () => {
      it("should return Question does not belong to this Quiz", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app)
          .put(`/api/v1/quizzes/${quizId2}/questions/${questionId}`)
          .send({ title: "updated question" });
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Question does not belong to this Quiz");
      });
    });
    describe("not found case of Quiz", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app)
          .put(`/api/v1/quizzes/${id}/questions/${id}`)
          .send({ title: "updated question" });
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Quiz not found");
      });
    });
    describe("not found case of Question", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app)
          .put(`/api/v1/quizzes/${quizId}/questions/${id}`)
          .send({ title: "updated question" });
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Question not found");
      });
    });
    describe("validation error (joi)", () => {
      it("should return a validation error", async () => {
        const id = new mongoose.Types.ObjectId();
        const { body, status } = await request(app)
          .put(`/api/v1/quizzes/${id}/questions/${id}`)
          .send({ isMultipleChoice: "unvalid value" });
         expect(status).toBe(422);
         expect(body.message).toBe("isMultipleChoice must be a boolean");
         expect(body.status).toBe("fail");
      });
    });
  });
  describe("DELETE A QUESTION", () => {
    describe("not found case of Quiz", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode, _body } = await request(app).delete(
          `/api/v1/quizzes/${id}/questions/${id}`
        );
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Quiz not found");
      });
    });
    describe("not found case of Question", () => {
      it("should return not found", async () => {
        const id = new mongoose.Types.ObjectId();
        const { statusCode , _body} = await request(app).delete(
          `/api/v1/quizzes/${quizId}/questions/${id}`
        );
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Question not found");
      });
    });

    describe("Question does not belong to this Quiz", () => {
      it("should return Question does not belong to this Quiz", async () => {

        const payloadQuiz2 = {
          title: "Quiz 2",
          description: "this is a quiz description",
          isPublished: false,
          categories: [],
        };

         const answer2 = await request(app)
        .post("/api/v1/quizzes")
        .send(payloadQuiz2)
        .set("Content-type", "application/json");

        quizId2 = answer2.body.data._id;

        const payloadQuestion = {
          title: "question 1",
          description: "this is a question description",
          isMultipleChoice: false
        };

        const { body } = await request(app)
          .post(`/api/v1/quizzes/${quizId}/questions`)
          .send(payloadQuestion)
          .set("Content-type", "application/json");

          questionId = body.data._id;

        const { statusCode, _body } = await request(app)
          .delete(`/api/v1/quizzes/${quizId2}/questions/${questionId}`);
        expect(statusCode).toBe(404);
        expect(_body.status).toBe("fail");
        expect(_body.message).toBe("Question does not belong to this Quiz");
      });
    });
  });
});
