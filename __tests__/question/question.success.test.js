const request = require("supertest");
const app = require("../../app");
const { db } = require("../../config");
const mongoose = require("mongoose");

const testDB_URI = db.atlas_test

var quizId;
var questionId;


/* Connecting to the database before all test. */
beforeAll(async () => {
  await mongoose.connect(testDB_URI
  );
});

/* Closing database connection after all test. */
afterAll(async () => {
  await mongoose.connection.close();
});

describe("QUESTION - SUCCESS", () => {
  it("POST/ question - success", async () => {
    const payloadQuiz = {
        title: "Quiz 2",
        description: "this is a quiz description",
        isPublished: false,
        categories: [],
      };
      const answer = await request(app)
        .post("/api/v1/quizzes")
        .send(payloadQuiz)
        .set("Content-type", "application/json");

        quizId = answer.body.data._id;

    const payload = {
      title: "question 1",
      description: "this is a question description",
      isMultipleChoice: false
    };
    const { statusCode, body } = await request(app)
      .post(`/api/v1/quizzes/${quizId}/questions`)
      .send(payload)
      .set("Content-type", "application/json");
    expect(statusCode).toBe(200);
    expect(body.status).toBe("success");
    expect(body.message).toBe("Question created successfully");
    expect(body.data).toMatchObject({
        title: "question 1",
        description: "this is a question description",
        isMultipleChoice: false
    });
    questionId = body.data._id;
  });
  it("GET/ questions - success", async () => {
    const { _body, statusCode } = await request(app).get(`/api/v1/quizzes/${quizId}/questions/`);
    expect(statusCode).toBe(200);
    expect(_body.status).toBe("success");
    expect(Array.isArray(_body.data)).toBe(true);
  });

  it("GET/ question - success", async () => {
    const { body, status } = await request(app).get(`/api/v1/quizzes/${quizId}/questions/${questionId}`);
    expect(status).toBe(200);
    expect(body.status).toBe("success");
    expect(body.data).toMatchObject({
      _id: questionId,
      title: "question 1",
      description: "this is a question description",
      isMultipleChoice: false
    });
  });

  it("PUT/ question - success", async () => {
    const payload = {
      title: "updated  question title",
      isMultipleChoice: true
    };
    const { statusCode, _body } = await request(app)
      .put(`/api/v1/quizzes/${quizId}/questions/${questionId}`)
      .send(payload)
      .set("Content-type", "application/json");
    expect(statusCode).toBe(200);
    expect(_body.status).toBe("success");
    expect(_body.message).toBe("Question updated successfully");
    expect(_body.data).toMatchObject({
        title: "updated  question title",
        isMultipleChoice: true,
        _id: questionId
    });
  });
  it("DELETE/ question - success", async () => {
    const { _body, statusCode } = await request(app).delete(
      `/api/v1/quizzes/${quizId}/questions/${questionId}`
    );
    expect(statusCode).toBe(200);
    expect(_body.status).toBe("success");
    expect(_body.message).toBe("Question deleted successfully");
  });
});
