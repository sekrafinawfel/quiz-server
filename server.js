const app = require("./app");
const config = require("./config");
const { connect } = require("./db/connectDB");
const PORT = config.port || 5001;
connect();

app.listen(PORT, () => {
  console.log(`app running on port ${PORT}`);
});
